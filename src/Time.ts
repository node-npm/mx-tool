/**
 * 单独封装一个时间相关的类，为了解决后期日期处理问题
 */
export class LocalDate extends Date {

    static Day = 24 * 60 * 60 * 1000;
    static Hour = 60 * 60 * 1000;
    static Minute = 60 * 1000;
    static Second = 1000;

    static now() {
        return Date.now();
    }

    static parse(s: string) {
        return Date.parse(s);
    }

    static formateString(t?: number) {
        let t1 = t ? new LocalDate(t) : new LocalDate();
        return t1.formateString();
    }

    /**
     * 负责解析时分秒，返回一个毫秒级的数字
     * @param s 23:30:15
     */
    static parseTime(s: string) {
        let exList = [LocalDate.Hour, LocalDate.Minute, LocalDate.Second]
        let iTime = 0;
        let asTime = s.split(':');
        for (let i = 0; i < asTime.length && i < exList.length; i++) {
            let n = parseInt(asTime[i]);
            if (isNaN(n)) n = 0;
            iTime += n * exList[i]
        }

        return iTime;
    }

    toOnlyTime() {
        return this.getHours() * LocalDate.Hour + this.getMinutes() * LocalDate.Minute + this.getSeconds() * LocalDate.Second
    }

    toLen(num: number, len: number) {
        let out = num.toString();
        while (out.length < len) out = '0' + out;
        return out;
    }

    formateString() {
        return `${this.toLen(this.getFullYear(), 4)}-${this.toLen(this.getMonth() + 1, 2)}-${this.toLen(this.getDate(), 2)} ${this.toLen(this.getHours(), 2)}:${this.toLen(this.getMinutes(), 2)}:${this.toLen(this.getSeconds(), 2)}`;
    }

    static isMonthly(timeA: number, timeB: number) {
        let tA = new LocalDate(timeA);
        let tB = new LocalDate(timeB);

        if (tA.getFullYear() == tB.getFullYear() &&
            tA.getMonth() == tB.getMonth()) {
            return true;
        }

        return false;
    }

    static isDaily(timeA: number, timeB: number) {
        let tA = new LocalDate(timeA);
        let tB = new LocalDate(timeB);

        if (tA.getFullYear() == tB.getFullYear() &&
            tA.getMonth() == tB.getMonth() &&
            tA.getDate() == tB.getDate()) {
            return true;
        }

        return false;
    }

    static isTime(timeA: number, timeB: number, type: "year" | "month" | "date" | "hour" | "minute" | "second") {
        let tA = new LocalDate(timeA);
        let tB = new LocalDate(timeB);

        switch (type) {
            default:
            case 'second':
                if (tA.getSeconds() != tB.getSeconds()) return false;
            case 'minute':
                if (tA.getMinutes() != tB.getMinutes()) return false;
            case 'hour':
                if (tA.getHours() != tB.getHours()) return false;
            case "date":
                if (tA.getDate() != tB.getDate()) return false;
            case "month":
                if (tA.getMonth() != tB.getMonth()) return false;
            case "year":
                if (tA.getFullYear() != tB.getFullYear()) return false;
                break;
        }

        return true;
    }


    static isYearly(timeA: number, timeB: number) {
        let tA = new LocalDate(timeA);
        let tB = new LocalDate(timeB);

        if (tA.getFullYear() == tB.getFullYear()) {
            return true;
        }

        return false;
    }

    static isWeekly(timeA: string | number, timeB: string | number) {
        let tA = new Date(timeA);
        tA.setHours(0, 0, 0, 0);
        let tB = new Date(timeB);
        tB.setHours(0, 0, 0, 0);

        let w1 = tA.getDay();
        if (w1 == 0) w1 = 7;
        let t1 = tA.getTime() - w1 * (1000 * 60 * 60 * 24);

        let w2 = tB.getDay();
        if (w2 == 0) w2 = 7;
        let t2 = tB.getTime() - w2 * (1000 * 60 * 60 * 24);

        if (t1 != t2) {
            return false
        }

        return true;
    }
}