import { http_quest } from "../src"
import assert from "assert";

describe("连接测试", function () {

    it("post", async function () {
        let rs = await http_quest("GET", "www.baidu.com", {}, 0, {}, { request_type: "text", timeout: 0 })
        assert.ok(rs);
    });

    it("get json", async function () {
        let rs = await http_quest<any[]>("GET", "http://mysafeinfo.com/api/data?list=englishmonarchs&format=json", {}, 0, {}, { respon_type: "json", timeout: 0 })
        assert.ok(rs.length);
    });

    it("post timeout", async function () {
        await assert.rejects(async () => {
            await http_quest("POST", "http://172.22.124.41:11111/", {}, 0, {}, { request_type: "json", timeout: 1 * 1000 });
        }, (err) => {
            assert.strictEqual(err.name, 'TimeoutError');
            assert.strictEqual(err.message, "Timeout awaiting 'request' for 1000ms");
            return true;
        });
    });

    it("fix header is null", async function () {
        let rs = await http_quest<any[]>("GET", "http://mysafeinfo.com/api/data?list=englishmonarchs&format=json", {}, 0, null, { respon_type: "json", timeout: 0 })
        assert.ok(rs.length);
    });

    it("fix no respon_type", async function () {
        let rs = await http_quest<any[]>("POST", "http://mysafeinfo.com/api/data?list=englishmonarchs&format=json", {
            "test": 1,
        }, 0, {}, { timeout: 0 })
        assert.ok(rs.length);
    });

    it("fix unnecessary param", async function () {
        let rs = await http_quest<any[]>("POST", "http://mysafeinfo.com/api/data?list=englishmonarchs&format=json");
        assert.ok(rs.length);
    });

})